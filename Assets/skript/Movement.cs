using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float JumpForce = 10;
    private Rigidbody2D playerRigidbody;
    private KeyCode jumpCode = KeyCode.Space;
    private Animator playerAnimator;
    private bool spriteFlip;
    private bool isGrounded;
  
    

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
      float horizontalInput = Input.GetAxis("Horizontal");
        Move(horizontalInput);
        if (Input.GetKeyDown(jumpCode) && isGrounded) Jump();
        Flip(horizontalInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask("Ground"));
            }

    private void Move(float direktion)
    {
        Vector2 velocity = new Vector2(speed * direktion, playerRigidbody.velocity.y);
        playerRigidbody.velocity = velocity;
        if (direktion != 0) playerAnimator.SetBool("Ran", true);
        else playerAnimator.SetBool("Ran", false);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(0f, JumpForce);
        playerRigidbody.velocity += jumpVector;
        isGrounded = false;
    }

    private void Flip(float diraktion)
    {
        bool needFlip = (diraktion > 0 && spriteFlip) || (diraktion < 0 && !spriteFlip);
        Vector3 scale = transform.localScale;
        if (needFlip)
        {
            spriteFlip = !spriteFlip;
            if (needFlip) transform.localScale = new Vector3(-1f * scale.x, scale.y, scale.z);
        }
    }
  


}    
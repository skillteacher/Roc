using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class None : MonoBehaviour
{
    [SerializeField] private string groundLauerName = "Ground";
    [SerializeField] private SimplEnome simplEnome;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLauerName)) return;
        simplEnome.Flip();
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplEnome : MonoBehaviour
{
     [SerializeField] private float speed = 5f;
    [SerializeField] private string groundLauerName = "Ground";
     private Rigidbody2D enomyRigidbody2D;
    private bool isFacingRight;

    private void Awake()
    {
        enomyRigidbody2D = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        Vector2 velosity =
           new Vector2( isFacingRight ? speed : -speed, enomyRigidbody2D.velocity.y);

        enomyRigidbody2D.velocity = velosity;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLauerName)) return;
        Flip();
    }
    public void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f,180f,0f);
    }

}
